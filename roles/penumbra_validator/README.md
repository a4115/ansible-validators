# Ansible role for Penumbra validator

Based on https://github.com/alchemydc/ansible-toolbox/tree/main/roles/penumbra_validator

## Setup

- Copy `./vars/main.yml.template` to `./vars/main.yml` and populate vars in it
- Copy `./vars/encrypted.yml.template` to `./vars/encrypted.yml`, populate vars in it and encrypt it
- Add  cometbft private key to `./files/priv_validator_key.json.encrypted`
- Copy `./templates/validator.toml.j2.template` to `./templates/validator.toml.j2.encrypted`, populate vars and encrypt it.
You can use `pcli validator template-definition`. See: https://guide.penumbra.zone/main/pd/join-testnet.html
- Review vars in `./defaults/`, specially `git_tag`.
