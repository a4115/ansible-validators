# Ansible role for Canto validator

Role Variables
--------------

- svc_user: linux user for running canto service
- repo: URL of Canto git repo
- git_tag: Tag to use for building Canto
- chain_id: Id of Canto chain
- SNAP_RPC: URL for state sync
- commission-max-change-rate:
- commission-max-rate:
- commission-rate:
- moniker: Your validator name

Requirements
------------
`jmespath` is required for JSON query filter

```
pip3 install jmespath
```

License
-------

MIT

Author Information
------------------

https://amamu.io
