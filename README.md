# Ansible validators

Collection of Ansible roles and playbooks to deploy validators

# Validator list

## Penumbra

Check `./roles/penumbra-validator/README.md`

Define `penumbra` host  in inventory `hosts` file.

```
ansible-playbook --vault-password-file password_file penumbra.yml
```

## Gnosis

Check `./roles/gnosis-validator/README.md`

Define `gnosis` host  in inventory `hosts` file.

```
ansible-playbook gnosis.yml
```

When everything is synced, upload validator keys, import them with:

```
sudo -u lighthouse /usr/bin/lighthouse --network gnosis account validator import --directory /path/to/validator_keys
```

and then start the validator with:

```
sudo service lighthouse_validator start
```

### Logging
```
journalctl -u nethermind -f
journalctl -u lighthouse_validator -f
```


# Canto

Check `./roles/canto-validator/README.md`

Define `canto` host  in inventory `hosts` file.

```
ansible-playbook canto.yml
```

Create your key with `cantod key add`, recover it from a mnemonic with `cantod key add <name> --recover` or import it with `cantod key import`.

Make sure your node is working and synced, and then create validator tx as explained here:

https://docs.canto.io/canto-node/validators/quickstart-guide#8.-create-validator-transaction
